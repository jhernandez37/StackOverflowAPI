# API de Preguntas de Stack Overflow
Esta aplicación proporciona un servicio para recuperar y almacenar preguntas de Stack Overflow utilizando su API pública.

# Funcionalidad
La aplicación realiza las siguientes operaciones:

- Recupera preguntas de Stack Overflow según los parámetros proporcionados.
- Almacena las preguntas en una base de datos local.
- Actualiza las preguntas existentes en la base de datos si ya están presentes.

# Requisitos Previos
Asegúrate de tener instalados los siguientes elementos antes de comenzar:

- PHP 7.4 o superior
- Composer (para la gestión de dependencias)
- Symfony CLI (para el desarrollo con Symfony)

# Instalación

Clona el repositorio:
```
git clone https://gitlab.com/jhernandez37/StackOverflowAPI.git
```
Navega al directorio del proyecto:
```
cd StackAPI
```

Instala las dependencias con Composer:
```
composer install
```

Configura la base de datos (si es necesario):
```
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
```

Inicia el servidor Symfony:
```
symfony server:start
```

# Uso
Accede a la API a través de la siguiente URL:
```
http://localhost:8000/questions?tag=etiqueta
```

Reemplaza "etiqueta" con la etiqueta de Stack Overflow que deseas buscar. Los parámetros "fromDate" y "toDate" son opcionales. Puedes utilizar el formato YYYY-MM-DD para filtrar por fecha.

# Parámetros
- tag (obligatorio): La etiqueta de Stack Overflow a buscar.
- fromDate (opcional): Fecha de inicio para filtrar preguntas (formato: YYYY-MM-DD).
- toDate (opcional): Fecha de fin para filtrar preguntas (formato: YYYY-MM-DD).


# Contribuciones
¡Las contribuciones son bienvenidas! Si encuentras algún problema o tienes una mejora, crea un issue o envía un pull request.

# Licencia
Este proyecto está bajo la licencia MIT.
