# Stack Overflow Questions API
This application provides a service to retrieve and store Stack Overflow questions using its public API.

# Functionality
The application performs the following operations:

- Retrieves Stack Overflow questions based on the provided parameters.
- Stores the questions in a local database.
- Updates existing questions in the database if they are already present.

# Prerequisites
Make sure you have the following installed before getting started:

- PHP 7.4 or higher
- Composer (for dependency management)
- Symfony CLI (for Symfony development)

# Installation

Clone the repository:
```
git clone https://gitlab.com/jhernandez37/StackOverflowAPI.git
```
Navigate to the project directory:
```
cd StackAPI
```
Install dependencies with Composer:
```
composer install
```

Configure the database (if necessary):
```
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
```

Start the Symfony server:
```
symfony server:start
```

# Usage
Access the API through the following URL:
```
http://localhost:8000/questions?tag=tag
```

Replace "tag" with the Stack Overflow tag you want to search. The "fromDate" and "toDate" parameters are optional. You can use the format YYYY-MM-DD to filter by date.

# Parameters
- tag (required): The Stack Overflow tag to search for.
- fromDate (optional): Start date to filter questions (format: YYYY-MM-DD).
- toDate (optional): End date to filter questions (format: YYYY-MM-DD).

# Contributions
Contributions are welcome! If you encounter any issues or have an improvement, create an issue or submit a pull request.

# License
This project is licensed under the MIT License.