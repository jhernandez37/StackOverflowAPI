<?php

namespace App\Entity;

use App\Repository\QuestionsRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: QuestionsRepository::class)]
class Questions
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(type: Types::ARRAY)]
    private array $tags = [];

    #[ORM\Column]
    private ?bool $in_answered = null;

    #[ORM\Column]
    private ?int $view_count = null;

    #[ORM\Column]
    private ?int $answer_count = null;

    #[ORM\Column]
    private ?int $score = null;

    #[ORM\Column]
    private ?int $creation_date = null;

    #[ORM\Column]
    private ?int $last_activity_date = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): static
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function setTags(array $tags): static
    {
        $this->tags = $tags;

        return $this;
    }

    public function isInAnswered(): ?bool
    {
        return $this->in_answered;
    }

    public function setInAnswered(bool $in_answered): static
    {
        $this->in_answered = $in_answered;

        return $this;
    }

    public function getViewCount(): ?int
    {
        return $this->view_count;
    }

    public function setViewCount(int $view_count): static
    {
        $this->view_count = $view_count;

        return $this;
    }

    public function getAnswerCount(): ?int
    {
        return $this->answer_count;
    }

    public function setAnswerCount(int $answer_count): static
    {
        $this->answer_count = $answer_count;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): static
    {
        $this->score = $score;

        return $this;
    }

    public function getCreationDate(): ?int
    {
        return $this->creation_date;
    }

    public function setCreationDate(int $creation_date): static
    {
        $this->creation_date = $creation_date;

        return $this;
    }

    public function getLastActivityDate(): ?int
    {
        return $this->last_activity_date;
    }

    public function setLastActivityDate(int $last_activity_date): static
    {
        $this->last_activity_date = $last_activity_date;

        return $this;
    }
}
