<?php

namespace App\Controller;

use App\Entity\Questions;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Routing\Annotation\Route;

class QuestionsController extends AbstractController
{
    private $serializer;
    private $httpClient;

    public function __construct(SerializerInterface $serializer, HttpClientInterface $httpClient)
    {
        $this->serializer = $serializer;
        $this->httpClient = $httpClient;
    }
    #[Route('/questions', name: 'app_questions')]
    public function getStackOverflowQuestions(
        Request $request,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
        HttpClientInterface $httpClient
    ): JsonResponse {
        $apiUrl = 'https://api.stackexchange.com/2.3/questions';

        // Get parameters from the request
        $tag = $request->query->get('tag');
        $fromDate = $request->query->get('fromdate');
        $toDate = $request->query->get('todate');

            // Check if 'tag' parameter is provided
        if (empty($tag)) {
            $errorResponse = [
                'error' => 'Tag parameter is required.',
            ];

            // Convert the error response to JSON
            $jsonErrorResponse = $serializer->serialize($errorResponse, 'json');

            // Create a JsonResponse for the error and return it
            return new JsonResponse($jsonErrorResponse, 400, [], true);
        }

        // Define the default parameters
        $parameters = [
            'order' => 'desc',
            'sort' => 'activity',
            'tagged' => $tag,
            'site' => 'stackoverflow',
        ];

        // Add fromDate and toDate if present
        if ($fromDate) {
            $parameters['fromdate'] = strtotime($fromDate);
        }

        if ($toDate) {
            $parameters['todate'] = strtotime($toDate);
        }

        // Make the request and get the response
        $response = $httpClient->request('GET', $apiUrl, ['query' => $parameters]);
        $data = $response->toArray();

        // Iterate through each item in the response
        foreach ($data['items'] as $item) {
            // Check if the question with the same title already exists
            $existingQuestion = $entityManager->getRepository(Questions::class)->findOneBy(['title' => $item['title']]);

            if ($existingQuestion) {
                // Update the existing question
                $existingQuestion
                    ->setTags($item['tags'] ?? [])
                    ->setInAnswered($item['is_answered'] ?? false)
                    ->setViewCount($item['view_count'] ?? 0)
                    ->setAnswerCount($item['answer_count'] ?? 0)
                    ->setScore($item['score'] ?? 0)
                    ->setCreationDate($item['creation_date'])
                    ->setLastActivityDate($item['last_activity_date']);
            } else {
                // Create a new question
                $question = new Questions();
                $question
                    ->setTitle($item['title'])
                    ->setTags($item['tags'] ?? [])
                    ->setInAnswered($item['is_answered'] ?? false)
                    ->setViewCount($item['view_count'] ?? 0)
                    ->setAnswerCount($item['answer_count'] ?? 0)
                    ->setScore($item['score'] ?? 0)
                    ->setCreationDate($item['creation_date'])
                    ->setLastActivityDate($item['last_activity_date']);

                // Persist the entity
                $entityManager->persist($question);
            }
        }

        // Flush changes to the database
        $entityManager->flush();

        // Convert the response data to JSON
        $jsonResponse = $serializer->serialize($data, 'json');

        // Create a JsonResponse and return it
        return new JsonResponse($jsonResponse, 200, [], true);
    }
}